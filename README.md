# Chaikin-Generator

A chaikin curve generator, written in C#.

![An example curve.](ExampleData/Wave.gif)

## Getting started
I haven't been able to pack this into a Nuget package yet, so pull down this repository and reference the `ChaikinGenerator` project in your project.

Using `ChaikinGenerator` is easy. Start by adding the appropriate `using` statements and creating an instance of it:

```csharp
using SBRL.Utilities;
using SBRL.Rendering.Chaikin;

// ...

ChaikinGenerator curve = new ChaikinGenerator();

// ...
```

Then add some control points to it however you like:

```csharp
curve.Add(new PointF(400, 760);
curve.Add(new PointF(463, 682), new PointF(492, 6281));
curve.Add(new PointF[] {
	new PointF(300, 450),
	new PointF(582, 4821)
});
curve.Add(new List<PointF> { new PointF(54582, 562), new PointF(672, 148) });
```

Set the order to render the curve to (the number of interpolation passes to make over the control points):

```csharp
curve.Order = 6;
```

Render the curve (unless you set the `AutoRender` property to `true`):

```csharp
curve.Render();
```

Do amazing things with the rendered points!

```csharp
MakeAwesomeStuff(curve.RenderedPoints);
foreach(PointF renderedPoint in curve)
	DoCoolThings(renderedPoint);
```

There are also some hidden secret utility methods that you might find useful too that this package exposes. Feel free to use them too!

## Command Line Interface
A CLI is also available for testing purposes. It renders lists of points as a chaikin curve, and then outputs them as an svg. It can be found in the [Chaikin-CLI folder](https://gitlab.com/sbrl/Chaikin-Generator/tree/master/Chaikin-CLI) - simply build the solution in Monodevelop or Visual Studio and and binary will be dropped into `Chaikin-CLI/bin/Debug/`.

## License
This project is currently under the [Mozilla Public License version 2.0](https://gitlab.com/sbrl/Chaikin-Generator/blob/master/LICENSE) ([tl;drLegal](https://tldrlegal.com/license/mozilla-public-license-2.0-(mpl-2)))
