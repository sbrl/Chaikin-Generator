﻿Chaikin Curve Generator
=======================
By Starbeamrainbowlabs

Renders a list of points as a chaikin curve at the specified order, and output the result as an svg.

Usage:

    [mono ]Chaikin.exe [options] 

Options:

    -h, --help          Show this message
    -f, -i, --filename  The filename to read points from. If not specified points will be read from the standard input.
    --order             The order of curve to generate. Default: 5.

Example list of points:

    40.0 76.0
    465.3 68.2
    49.2 628.1
    30.0 45.0
    482.1 58.2
