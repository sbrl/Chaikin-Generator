﻿using System;
using System.IO;

using SBRL.Utilities;
using SBRL.Rendering.Chaikin;

namespace SBRL.Rendering.Chaikin.CLI
{
	class MainClass
	{
		public static int Main(string[] args)
		{
			try
			{
				Cli(args);
			}
			catch(Exception error)
			{
				Console.WriteLine("Error: {0}", error.Message);
				return 1;
			}
			return 0;
		}

		public static void Cli(string[] args)
		{
			ChaikinCurve curve = new ChaikinCurve();
			string filename = string.Empty;

			for (int i = 0; i < args.Length; i++)
			{
				switch(args[i].Trim().ToLower())
				{
					case "-h":
					case "--help":
						Console.WriteLine(EmbeddedFiles.ReadAllText("Chaikin.CLI.Embed.Help.md"));
						return;
						
					case "-i":
					case "-f":
					case "--filename":
						filename = args[++i];
						break;
					
					case "--order":
						curve.Order = int.Parse(args[++i]);
						break;
				}
			}

			StreamReader sourceStream;
			if (filename != string.Empty)
				sourceStream = new StreamReader(filename);
			else
				sourceStream = new StreamReader(Console.OpenStandardInput());

			curve.Add(PointFTools.FromStream(sourceStream));

			curve.Render();

			Console.WriteLine(curve.AsSvg());
		}
	}
}
