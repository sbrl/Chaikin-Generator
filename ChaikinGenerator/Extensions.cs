﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace SBRL.Utilities
{
	static class Extensions
	{
		public static List<T> DeepClone<T>(this IList<T> listToClone) where T : ICloneable
		{
			return listToClone.Select(item => (T)item.Clone()).ToList();
		}
		public static List<T> Clone<T>(this IList<T> listToClone) where T : struct
		{
			return listToClone.Select(item => (T)item).ToList();
		}

		public static PointF Multiply(this PointF a, PointF b)
		{
			return new PointF(a.X * b.X, a.Y * b.Y);
		}
		public static PointF Multiply(this PointF a, float b)
		{
			return new PointF(a.X * b, a.Y * b);
		}

		public static PointF Add(this PointF a, PointF b)
		{
			return new PointF(a.X + b.X, a.Y + b.Y);
		}

		public static PointF Subtract(this PointF a, PointF b)
		{
			return new PointF(a.X - b.X, a.Y - b.Y);
		}

		/// <summary>
		/// Returns a raw string representation (the numbers without any extra puncutation)
		/// of the PointF instance.
		/// Example: "4.573 -36.247"
		/// </summary>
		/// <returns>The raw string representation of the current PointF instance.</returns>
		public static string ToRawString(this PointF point)
		{
			return $"{point.X} {point.Y}";
		}
	}
}

