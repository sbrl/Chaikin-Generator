﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SBRL.Utilities
{
	public static class PointFTools
	{
		public static List<PointF> FromFile(string filename)
		{
			return FromStream(new StreamReader(filename));
		}
		
		public static List<PointF> FromStream(StreamReader sourceStream)
		{
			List<PointF> result = new List<PointF>();

			string nextLine = string.Empty;
			while((nextLine = sourceStream.ReadLine()) != null)
			{
				// Skip empty lines
				if (nextLine.Trim() == string.Empty) continue;

				result.Add(PointFTools.Parse(nextLine));
			}

			return result;
		}

		public static PointF Parse(string source)
		{
			string[] parts = Regex.Split(source.Trim(), @"\s+");
			if (parts.Length != 2)
				throw new InvalidDataException($"Invalid source data: Can't convert '{source}' to a PointF.");

			try
			{
				return new PointF(
					float.Parse(parts[0]),
					float.Parse(parts[1])
				);
			}
			catch(Exception error)
			{
				throw new InvalidDataException($"Error converting '{source}' as a PointF.", error);
			}
		}
	}
}
