﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

using SBRL.Utilities;

namespace SBRL.Rendering.Chaikin
{
	public class ChaikinCurve : IEnumerable<PointF>
	{
		private int order = 5;
		private bool closed = false;
		/// <summary>
		/// The width to use when rendering the curve as an SVG file.
		/// </summary>
		private int svgStrokeWidth = 3;

		/// <summary>
		/// The points in the curve.
		/// </summary>
		private List<PointF> points = new List<PointF>();

		/// <summary>
		/// Whether the curve should be automatically rendered whenever anything changes.
		/// </summary>
		public bool AutoRender = false;

		/// <summary>
		/// Whether the curve should be closed or not.
		/// Note: This property has not been implemented yet.
		/// </summary>
		public bool Closed
		{
			get { return closed; }
			set { closed = value; if(AutoRender) Render(); }
		}

		/// <summary>
		/// The level of detail to render the curve to.
		/// </summary>
		/// <value>The order.</value>
		public int Order {
			get { return order; }
			set { order = value; if(AutoRender) Render(); }
		}

		/// <summary>
		/// A list of points that represent the rendered curve.
		/// Not populated until Render() is called.
		/// </summary>
		public List<PointF> RenderedPoints = new List<PointF>();

		/// <summary>
		/// Calculates the appropriate SVG viewBox for the curve.
		/// </summary>
		private string svgViewBox {
			get {
				return $"{BoundingBox.X - (svgStrokeWidth / 2)} {BoundingBox.Y - (svgStrokeWidth / 2)} " +
					$"{BoundingBox.Width + svgStrokeWidth} {BoundingBox.Height + svgStrokeWidth}";
			}
		}

		/// <summary>
		/// The bounding box of the curve in question.
		/// This value is dynamically calculated by using the RenderedPoints list,
		/// so you probably want to call Render() first.
		/// </summary>
		public RectangleF BoundingBox {
			get {
				PointF min = new PointF(
					RenderedPoints.Min(point => point.X),
					RenderedPoints.Min(point => point.Y)
				);
				PointF max = new PointF(
					RenderedPoints.Max(point => point.X),
					RenderedPoints.Max(point => point.Y)
				);

				return new RectangleF(min, new SizeF(max.X - min.X, max.Y - min.Y));
			}
		}

		/// <summary>
		/// Creates a new Chaikin curve.
		/// </summary>
		public ChaikinCurve()
		{
		}

		/// <summary>
		/// Adds a list of points to the curve.
		/// </summary>
		/// <param name="newPoints">The new points to add.</param>
		public void Add(List<PointF> newPoints)
		{
			Add(newPoints.ToArray());
		}
		/// <summary>
		/// Add the specified points.
		/// </summary>
		/// <param name="newPoints">The new points to add.</param>
		public void Add(params PointF[] newPoints)
		{
			points.AddRange(newPoints);

			if(AutoRender) Render();
		}

		/// <summary>
		/// Recalculates the smooth curve specified by the control points provided to the current order.
		/// </summary>
		public List<PointF> Render()
		{
			RenderedPoints = points.Clone();

			for(int pass = 0; pass < Order; pass++)
			{
				// TODO: Make this work for closed curves too
				for(int i = 0; i < RenderedPoints.Count - 1; i += 2) // Stop one from the end
				{
					PointF start = RenderedPoints[i], end = RenderedPoints[i + 1];
					RenderedPoints[i] = Interpolate(start, end, 0.25f);
					RenderedPoints.Insert(i + 1, Interpolate(start, end, 0.75f));
				}
				RenderedPoints.RemoveAt(RenderedPoints.Count - 1);
			}

			return RenderedPoints;
		}

		/// <summary>
		/// Outputs the curve as a string of SVG.
		/// Utilises the RenderedPoints array, you'll  probably want to call Render() first.
		/// </summary>
		/// <returns>The curve as a string of svg.</returns>
		public string AsSvg()
		{
			string namespaceSvg = "http://www.w3.org/2000/svg";
			MemoryStream svgDocument = new MemoryStream();
			XmlWriterSettings settings = new XmlWriterSettings() { Encoding = Encoding.UTF8 };
			XmlWriter svg = XmlWriter.Create(svgDocument, settings);
			svg.WriteStartDocument();
			svg.WriteStartElement("svg", namespaceSvg);
			svg.WriteAttributeString("version", "1.2");
			svg.WriteAttributeString("baseProfile", "tiny");
			svg.WriteAttributeString("viewBox", svgViewBox);
			svg.WriteAttributeString("width", BoundingBox.Width.ToString());
			svg.WriteAttributeString("height", BoundingBox.Height.ToString());

			svg.WriteElementString("title", "Chaikin Curve");
			svg.WriteElementString("desc", $"Chaikin Curve generated at {DateTime.Now} " +
				"by SBRLUtilities.ChaikinCurve, " +
				"which was written by Starbeamrainbowlabs (https://starbeamrainbowlabs.com).");

			svg.WriteStartElement("path");
			svg.WriteAttributeString("fill", "none");
			svg.WriteAttributeString("stroke", "black");
			svg.WriteAttributeString("stroke-width", svgStrokeWidth.ToString());
			svg.WriteAttributeString("d", asSvgPathInstructions());
			svg.WriteEndElement();

			svg.Close();

			return Encoding.UTF8.GetString(svgDocument.ToArray());
		}

		/// <summary>
		/// Interpolates a specified amount between two specified points.
		/// </summary>
		/// <param name="start">The starting point.</param>
		/// <param name="end">The ending point.</param>
		/// <param name="time">The percentage between 0 and 1 to interpolate to.</param>
		private PointF Interpolate(PointF start, PointF end, float time)
		{
			return end.Subtract(start).Multiply(time).Add(start);
		}

		/// <summary>
		/// Converts the currently rendered points to SVG path instructions.
		/// </summary>
		/// <returns>The currently rendered points as a set of svg path instructions.</returns>
		private string asSvgPathInstructions()
		{
			string result = $"M {RenderedPoints[0].ToRawString()}";

			foreach(PointF point in RenderedPoints.Skip(1))
				result += $" L {point.ToRawString()}";

			return result;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return (IEnumerator)RenderedPoints.GetEnumerator();
		}
		IEnumerator<PointF> IEnumerable<PointF>.GetEnumerator()
		{
			return RenderedPoints.GetEnumerator();
		}
	}
}
